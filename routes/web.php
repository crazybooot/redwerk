<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group([
    'as' => 'front',
], function () {
    Route::get('/', [
        'as'   => '.index',
        'uses' => 'FrontController@index',
    ]);
    Route::post('/', [
        'as'   => '.contact',
        'uses' => 'FrontController@contact',
    ]);
});

/**
 * Auth routes group
 */
Route::group([
    'prefix'    => 'admin',
    'as'        => 'auth',
    'namespace' => 'Auth',
], function () {
    Route::get('login', [
        'as'   => '.form',
        'uses' => 'LoginController@showLoginForm',
    ]);
    Route::post('login', [
        'as'   => '.login',
        'uses' => 'LoginController@login',
    ]);
    Route::get('logout', [
        'as'   => '.logout',
        'uses' => 'LoginController@logout',
    ]);
});

/**
 * Admin routes group
 */
Route::group([
    'prefix'     => 'admin',
    'as'         => 'admin',
    'namespace'  => 'Admin',
    'middleware' => 'auth',
], function () {
    Route::get('/', [
        'as'   => '.index',
        'uses' => 'PageController@index',
    ]);

    /**
     * Routes group for pages management
     */
    Route::group([
        'prefix' => 'page',
        'as'     => '.page',
    ], function () {
        Route::get('/', [
            'as'   => '.index',
            'uses' => 'PageController@index',
        ]);
        Route::get('create', [
            'as'   => '.create',
            'uses' => 'PageController@create',
        ]);
        Route::post('/', [
            'as'   => '.store',
            'uses' => 'PageController@store',
        ]);
        Route::get('{page}/edit', [
            'as'   => '.edit',
            'uses' => 'PageController@edit',
        ]);
        Route::put('{page}', [
            'as'   => '.update',
            'uses' => 'PageController@update',
        ]);
        Route::delete('{page}', [
            'as'   => '.destroy',
            'uses' => 'PageController@destroy',
        ]);
    });

    /**
     * Routes group for slides management
     */
    Route::group([
        'prefix' => 'slide',
        'as'     => '.slide',
    ], function () {
        Route::get('/', [
            'as'   => '.index',
            'uses' => 'SlideController@index',
        ]);
        Route::get('create', [
            'as'   => '.create',
            'uses' => 'SlideController@create',
        ]);
        Route::post('/', [
            'as'   => '.store',
            'uses' => 'SlideController@store',
        ]);
        Route::get('{slide}/edit', [
            'as'   => '.edit',
            'uses' => 'SlideController@edit',
        ]);
        Route::put('{slide}', [
            'as'   => '.update',
            'uses' => 'SlideController@update',
        ]);
        Route::delete('{slide}', [
            'as'   => '.destroy',
            'uses' => 'SlideController@destroy',
        ]);
    });

    /**
     * Routes group for header links management
     */
    Route::group([
        'prefix' => 'header_link',
        'as'     => '.header_link',
    ], function () {
        Route::get('/', [
            'as'   => '.index',
            'uses' => 'HeaderLinkController@index',
        ]);
        Route::get('create', [
            'as'   => '.create',
            'uses' => 'HeaderLinkController@create',
        ]);
        Route::post('/', [
            'as'   => '.store',
            'uses' => 'HeaderLinkController@store',
        ]);
        Route::get('{link}/edit', [
            'as'   => '.edit',
            'uses' => 'HeaderLinkController@edit',
        ]);
        Route::put('{link}', [
            'as'   => '.update',
            'uses' => 'HeaderLinkController@update',
        ]);
        Route::delete('{link}', [
            'as'   => '.destroy',
            'uses' => 'HeaderLinkController@destroy',
        ]);
    });

    /**
     * Routes group for footer links management
     */
    Route::group([
        'prefix' => 'footer_link',
        'as'     => '.footer_link',
    ], function () {
        Route::get('/', [
            'as'   => '.index',
            'uses' => 'FooterLinkController@index',
        ]);
        Route::get('create', [
            'as'   => '.create',
            'uses' => 'FooterLinkController@create',
        ]);
        Route::post('/', [
            'as'   => '.store',
            'uses' => 'FooterLinkController@store',
        ]);
        Route::get('{link}/edit', [
            'as'   => '.edit',
            'uses' => 'FooterLinkController@edit',
        ]);
        Route::put('{link}', [
            'as'   => '.update',
            'uses' => 'FooterLinkController@update',
        ]);
        Route::delete('{link}', [
            'as'   => '.destroy',
            'uses' => 'FooterLinkController@destroy',
        ]);
    });
});
