<?php

namespace Redwerk\Providers;

use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Illuminate\Support\ServiceProvider;

/**
 * Class LocalEnvironmentServiceProvider
 *
 * @package Redwerk\Providers
 */
class LocalEnvironmentServiceProvider extends ServiceProvider
{
    /**
     * Local environment service providers
     *
     * @var array
     */
    protected $localProviders = [
        IdeHelperServiceProvider::class,
    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if (app()->environment('local')) {
            $this->registerServiceProviders();
        }
    }

    /**
     * Register local environment service providers
     */
    protected function registerServiceProviders()
    {
        foreach ($this->localProviders as $provider) {
            $this->app->register($provider);
        }
    }
}
