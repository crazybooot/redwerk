<?php

namespace Redwerk\Providers;

use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Collective\Html\{
    FormFacade,
    HtmlFacade,
    HtmlServiceProvider
};
use Illuminate\{
    Foundation\AliasLoader,
    Support\ServiceProvider
};

/**
 * Class LibrariesServiceProvider
 *
 * @package Redwerk\Providers
 */
class LibrariesServiceProvider extends ServiceProvider
{
    /**
     * Custom service providers
     *
     * @var array
     */
    protected $customProviders = [
        HtmlServiceProvider::class,
        IdeHelperServiceProvider::class,
    ];

    /**
     * Custom facade aliases
     *
     * @var array
     */
    protected $customFacadeAliases = [
        'Form' => FormFacade::class,
        'Html' => HtmlFacade::class,
    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerCustomProviders();
        $this->registerFacadeAliases();
    }

    /**
     * Register custom service providers
     */
    protected function registerCustomProviders()
    {
        foreach ($this->customProviders as $customProvider) {
            $this->app->register($customProvider);
        }
    }

    /**
     * Register custom facade aliases
     */
    protected function registerFacadeAliases()
    {
        $loader = AliasLoader::getInstance();
        foreach ($this->customFacadeAliases as $alias => $facade) {
            $loader->alias($alias, $facade);
        }
    }
}
