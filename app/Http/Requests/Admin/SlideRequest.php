<?php

namespace Redwerk\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class SlideRequest
 *
 * @package Redwerk\Http\Requests\Admin
 */
class SlideRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'        => 'sometimes|string|min:3|max:255',
            'text'         => 'sometimes|string|min:3|max:255',
            'button_text'  => 'sometimes|string|min:3|max:255',
            'button_link'  => 'sometimes|string|min:3|max:255',
            'order'        => 'sometimes|integer|min:0',
            'is_published' => 'sometimes|boolean',
            'image'        => 'required|file|mimes:jpeg,jpg,png|max:2048',
        ];
    }
}
