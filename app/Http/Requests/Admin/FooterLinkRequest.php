<?php

namespace Redwerk\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class FooterLinkRequest
 *
 * @package Redwerk\Http\Requests\Admin
 */
class FooterLinkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'        => 'required|string|min:3|max:255',
            'url'          => 'required|string|min:3|max:255',
            'order'        => 'sometimes|integer|min:0',
            'is_published' => 'sometimes|boolean',
        ];
    }
}
