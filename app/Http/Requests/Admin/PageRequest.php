<?php

namespace Redwerk\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class PageRequest
 *
 * @package Redwerk\Http\Requests\Admin
 */
class PageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'html_title'       => 'sometimes|string|min:3|max:255',
            'html_h1'          => 'sometimes|string|min:3|max:255',
            'meta_description' => 'sometimes|string|min:3|max:255',
            'meta_keywords'    => 'sometimes|string|min:3|max:255',
            'seo_url'          => 'required|string|min:3|max:255',
            'title'            => 'required|string|min:3|max:255',
            'content'          => 'required|string',
            'is_published'     => 'sometimes|boolean',
        ];
    }
}
