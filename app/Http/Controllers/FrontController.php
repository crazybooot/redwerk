<?php

namespace Redwerk\Http\Controllers;

use Illuminate\{
    Http\RedirectResponse,
    View\View
};
use Mail;
use Redwerk\{
    Http\Requests\GetInTouchRequest,
    Mail\GetInTouchSubmited
};
use Redwerk\Models\{
    Budget,
    FooterLink,
    HeaderLink,
    Slide,
    Subscription
};

/**
 * Class FrontController
 *
 * @package Redwerk\Http\Controllers
 */
class FrontController extends Controller
{

    /**
     * Show index page
     *
     * @return View
     */
    public function index(): View
    {
        return view('front.index', [
            'header_links' => HeaderLink::published()->orderBy('order')->get(),
            'footer_links' => FooterLink::published()->orderBy('order')->get(),
            'slides'       => Slide::published()->orderBy('order')->get(),
            'budgets'      => Budget::all(),
        ]);
    }

    /**
     * Handle get in touch form
     *
     * @param GetInTouchRequest $request
     * @return RedirectResponse
     */
    public function getInTouch(GetInTouchRequest $request): RedirectResponse
    {
        if ($request->input('subscribe')) {
            Subscription::create(['email' => $request->input('email')]);
        }
        //TODO change path to admin mail
        Mail::to(config('config.admin.mail'))->send(new GetInTouchSubmited($request->all()));

        return redirect()->route('front.index');
    }
}
