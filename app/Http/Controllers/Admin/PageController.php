<?php

namespace Redwerk\Http\Controllers\Admin;

use Illuminate\{
    Http\RedirectResponse,
    View\View
};
use Redwerk\Http\{
    Controllers\Controller,
    Requests\Admin\PageRequest
};
use Redwerk\Models\Page;

/**
 * Class PageController
 *
 * @package Redwerk\Http\Controllers\Admin
 */
class PageController extends Controller
{
    /**
     * Return list of pages
     *
     * @return View
     */
    public function index(): View
    {
        return view('admin.page.index', ['pages' => Page::all()]);
    }

    /**
     * Return new page creation form
     *
     * @return View
     */
    public function create(): View
    {
        return view('admin.page.create');
    }

    /**
     * Store new page
     *
     * @param PageRequest $request
     * @return RedirectResponse
     */
    public function store(PageRequest $request): RedirectResponse
    {
        Page::create($request->all());

        return redirect()->route('admin.page.index')
            ->with(['success' => trans('admin.page.create.status.success')]);
    }

    /**
     * Return edit page form
     *
     * @param Page $page
     * @return View
     */
    public function edit(Page $page): View
    {
        return view('admin.page.edit', ['page' => $page]);
    }

    /**
     * Update page
     *
     * @param Page $page
     * @param PageRequest $request
     * @return RedirectResponse
     */
    public function update(Page $page, PageRequest $request): RedirectResponse
    {
        $page->update($request->all());

        return redirect()->route('admin.page.index')
            ->with(['success' => trans('admin.page.update.status.success')]);
    }

    /**
     * Destroy page
     *
     * @param Page $page
     * @return RedirectResponse
     */
    public function destroy(Page $page): RedirectResponse
    {
        $page->delete();

        return redirect()->route('admin.page.index')
            ->with(['success' => trans('admin.page.delete.status.success')]);
    }
}
