<?php

namespace Redwerk\Http\Controllers\Admin;

use Illuminate\{
    Http\RedirectResponse,
    View\View
};
use Redwerk\Http\{
    Controllers\Controller,
    Requests\Admin\SlideRequest
};
use Redwerk\Models\Slide;

/**
 * Class SlideController
 *
 * @package Redwerk\Http\Controllers\Admin
 */
class SlideController extends Controller
{
    /**
     * Return list of slides
     *
     * @return View
     */
    public function index(): View
    {
        return view('admin.slide.index', ['slides' => Slide::all()]);
    }

    /**
     * Return new slide creation form
     *
     * @return View
     */
    public function create(): View
    {
        return view('admin.slide.create');
    }

    /**
     * Save new slide
     *
     * @param SlideRequest $request
     * @return RedirectResponse
     */
    public function store(SlideRequest $request): RedirectResponse
    {
        Slide::create($request->all());

        return redirect()->route('admin.slide.index')
            ->with(['success' => trans('admin.slide.create.status.success')]);
    }

    /**
     * Return slide edition form
     *
     * @param Slide $slide
     * @return View
     */
    public function edit(Slide $slide): View
    {
        return view('admin.slide.edit', ['slide' => $slide]);
    }

    /**
     * Update slide
     *
     * @param Slide $slide
     * @param SlideRequest $request
     * @return RedirectResponse
     */
    public function update(Slide $slide, SlideRequest $request): RedirectResponse
    {
        $slide->update($request->all());

        return redirect()->route('admin.slide.index')
            ->with(['success' => trans('admin.slide.update.status.success')]);
    }

    /**
     * Delete slide
     *
     * @param Slide $slide
     * @return RedirectResponse
     */
    public function destroy(Slide $slide): RedirectResponse
    {
        $slide->delete();

        return redirect()->route('admin.slide.index')
            ->with(['success' => trans('admin.slide.delete.status.success')]);
    }
}
