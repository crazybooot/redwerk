<?php

namespace Redwerk\Http\Controllers\Admin;

use Illuminate\{
    Http\RedirectResponse,
    View\View
};
use Redwerk\Http\{
    Controllers\Controller,
    Requests\Admin\FooterLinkRequest
};
use Redwerk\Models\FooterLink;

/**
 * Class FooterLinkController
 *
 * @package Redwerk\Http\Controllers\Admin
 */
class FooterLinkController extends Controller
{
    /**
     * Return list of footer links
     *
     * @return View
     */
    public function index(): View
    {
        return view('admin.footer_link.index', ['footer_inks' => FooterLink::all()]);
    }

    /**
     * Return new footer link creation form
     *
     * @return View
     */
    public function create(): View
    {
        return view('admin.footer_link.create');
    }

    /**
     * Save new footer link
     *
     * @param FooterLinkRequest $request
     * @return RedirectResponse
     */
    public function store(FooterLinkRequest $request): RedirectResponse
    {
        FooterLink::create($request->all());

        return redirect()->route('admin.footer_link.index')
            ->with(['success' => trans('admin.footer_link.create.status.success')]);
    }

    /**
     * Return footer link edition form
     *
     * @param FooterLink $link
     * @return View
     */
    public function edit(FooterLink $link): View
    {
        return view('admin.footer_link.edit', ['footer_link' => $link]);
    }

    /**
     * Update footer link
     *
     * @param FooterLink $link
     * @param FooterLinkRequest $request
     * @return RedirectResponse
     */
    public function update(FooterLink $link, FooterLinkRequest $request): RedirectResponse
    {
        $link->update($request->all());

        return redirect()->route('admin.footer_link.index')
            ->with(['success' => trans('admin.footer_link.update.status.success')]);
    }

    /**
     * Delete footer link
     *
     * @param FooterLink $link
     * @return RedirectResponse
     */
    public function destroy(FooterLink $link): RedirectResponse
    {
        $link->delete();

        return redirect()->route('admin.footer_link.index')
            ->with(['success' => trans('admin.footer_link.delete.status.success')]);
    }
}
