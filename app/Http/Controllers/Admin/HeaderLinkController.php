<?php

namespace Redwerk\Http\Controllers\Admin;

use Illuminate\{
    Http\RedirectResponse,
    View\View
};
use Redwerk\Http\{
    Controllers\Controller,
    Requests\Admin\HeaderLinkRequest
};
use Redwerk\Models\HeaderLink;

/**
 * Class HeaderLinkController
 *
 * @package Redwerk\Http\Controllers\Admin
 */
class HeaderLinkController extends Controller
{
    /**
     * Return list of header links
     *
     * @return View
     */
    public function index(): View
    {
        return view('admin.header_link.index', ['header_links' => HeaderLink::all()]);
    }

    /**
     * Return new header link creation form
     *
     * @return View
     */
    public function create(): View
    {
        return view('admin.header_link.create');
    }

    /**
     * Store new header link
     *
     * @param HeaderLinkRequest $request
     * @return RedirectResponse
     */
    public function store(HeaderLinkRequest $request): RedirectResponse
    {
        HeaderLink::create($request->all());

        return redirect()->route('admin.header_link.index')
            ->with(['success' => trans('admin.header_link.create.status.success')]);
    }

    /**
     * Return header link edition form
     *
     * @param HeaderLink $link
     * @return View
     */
    public function edit(HeaderLink $link): View
    {
        return view('admin.header_link.edit', ['header_link' => $link]);
    }

    /**
     * Update header link
     *
     * @param HeaderLink $link
     * @param HeaderLinkRequest $request
     * @return RedirectResponse
     */
    public function update(HeaderLink $link, HeaderLinkRequest $request): RedirectResponse
    {
        $link->update($request->all());

        return redirect()->route('admin.header_link.index')
            ->with(['success' => trans('admin.header_link.updae.status.success')]);
    }

    /**
     * Delete header link
     *
     * @param HeaderLink $link
     * @return RedirectResponse
     */
    public function destroy(HeaderLink $link): RedirectResponse
    {
        $link->delete();

        return redirect()->route('admin.header_link.index')
            ->with(['success' => trans('admin.header_link.delete.status.success')]);
    }
}
