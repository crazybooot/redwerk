<?php

namespace Redwerk\Models;

use Illuminate\Database\Eloquent\{
    Builder,
    Model
};

/**
 * Class Page
 *
 * @package Redwerk
 */
class Page extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'html_title',
        'html_h1',
        'meta_description',
        'meta_keywords',
        'seo_url',
        'title',
        'content',
        'is_published',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'html_title'       => 'string',
        'html_h1'          => 'string',
        'meta_description' => 'string',
        'meta_keywords'    => 'string',
        'seo_url'          => 'string',
        'title'            => 'string',
        'content'          => 'string',
        'is_published'     => 'boolean',
    ];

    /**
     * Return only published pages
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopePublished(Builder $query): Builder
    {
        return $query->where('is_published', true);
    }
}
