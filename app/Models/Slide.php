<?php

namespace Redwerk\Models;

use Illuminate\Database\Eloquent\{
    Builder,
    Model
};
use Spatie\MediaLibrary\HasMedia\{
    Interfaces\HasMediaConversions,
    HasMediaTrait
};

/**
 * Class Slide
 *
 * @package Redwerk\Models
 */
class Slide extends Model implements HasMediaConversions
{
    use HasMediaTrait;

    const MEDIACOLLECTION = 'slides';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'text',
        'button_text',
        'button_link',
        'order',
        'is_published',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'title'        => 'string',
        'text'         => 'string',
        'button_text'  => 'string',
        'button_link'  => 'string',
        'order'        => 'integer',
        'is_published' => 'boolean',
    ];

    /**
     * Return only published slides
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopePublished(Builder $query): Builder
    {
        return $query->where('is_published', true);
    }

    /**
     * Register the conversions that should be performed.
     *
     */
    public function registerMediaConversions()
    {
        $this->addMediaConversion('thumb')
            ->setManipulations(['w' => 60, 'h' => 60])
            ->performOnCollections(self::MEDIACOLLECTION);

        $this->addMediaConversion('big')
            ->setManipulations(['w' => 500, 'h' => 500])
            ->performOnCollections(self::MEDIACOLLECTION);
    }

    /**
     * Return url for big slide image
     *
     * @return null|string
     */
    public function getBigImageAttribute()
    {
        return $this->getFirstMediaUrl(self::MEDIACOLLECTION, 'big') ?? null;
    }

    /**
     * Return url for slide thumb image
     *
     * @return null|string
     */
    public function getThumbImageAttribute()
    {
        return $this->getFirstMediaUrl(self::MEDIACOLLECTION, 'thumb') ?? null;
    }
}
