<?php

namespace Redwerk\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Subscription
 *
 * @package Redwerk\Models
 */
class Subscription extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email' => 'string',
    ];
}
