<?php

namespace Redwerk\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Budget
 *
 * @package Redwerk\Models
 */
class Budget extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
    ];
}
