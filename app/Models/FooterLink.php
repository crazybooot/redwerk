<?php

namespace Redwerk\Models;

use Illuminate\Database\Eloquent\{
    Builder,
    Model
};

/**
 * Class FooterLink
 *
 * @package Redwerk\Models
 */
class FooterLink extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'url',
        'order',
        'is_published',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'title'        => 'string',
        'url'          => 'string',
        'order'        => 'integer',
        'is_published' => 'boolean',
    ];

    /**
     * Return only published links
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopePublished(Builder $query): Builder
    {
        return $query->where('is_published', true);
    }
}
