<nav>
    <div class="container">
                <span>
                    <img src="{!! asset('images/logo.png') !!}">
                </span>
        <span class="slogan uppercase">
                    Custom software development company
                </span>
        <div class="menu hidden-sm-down float-xl-right">
            <ul>
                <li><a href="#">about</a></li>
                <li><a href="#">services</a></li>
                <li><a href="#">case studies</a></li>
                <li><a href="#">testimonials</a></li>
                <li><a href="#">blog</a></li>
                <li><a href="#">contact</a></li>
            </ul>
        </div>
    </div>
</nav>