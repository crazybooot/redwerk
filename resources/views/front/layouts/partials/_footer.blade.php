<footer>
    <div class="container">
                <span>
                    <img src="{!! asset('images/logo-footer.png') !!}">
                </span>
        <span class="copywrite uppercase">
                    © 2016 Redwerk.  All rights reserved.
                </span>
        <div class="float-xl-right">
            <ul>
                <li><a href="#">about</a></li>
                <li><a href="#">services</a></li>
                <li><a href="#">case studies</a></li>
                <li><a href="#">testimonials</a></li>
                <li><a href="#">blog</a></li>
                <li><a href="#">contact</a></li>
            </ul>
        </div>
    </div>
</footer>
