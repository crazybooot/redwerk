@extends('front.layouts.main')
@section('title', trans('front.page.index.page_title'))
@section('content')
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="owl-carousel">
                        <div class="item">
                            <h2>we offer</h2>
                            <h4>quality software development outsourcing services </br>
                                at a reasonable price. </h4>
                            <button class="btn alert-btn"><a href="#">about us</a></button>
                        </div>
                        <div class="item">
                            <h2>we offer</h2>
                            <h4>quality software development outsourcing services </br>
                                at a reasonable price. </h4>
                            <button class="btn alert-btn"><a href="#">about us</a></button>
                        </div>
                        <div class="item">
                            <h2>we offer</h2>
                            <h4>quality software development outsourcing services </br>
                                at a reasonable price. </h4>
                            <button class="btn alert-btn"><a href="#">about us</a></button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-wrapper">
                        <img src="{!! asset('images/wave.png') !!}">
                        <h3>get in touch</br>with our team</h3>
                        <form>
                            <div class="form-group col-md-6">
                                <label>wef</label>
                                <input></input>
                            </div>
                            <div class="form-group col-md-6">
                                <label>wef</label>
                                <input></input>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection