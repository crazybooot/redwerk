@include('admin.layouts.partials._assets')
<!DOCTYPE html>
<html lang="{!! app()->getLocale() !!}">
<head>
    @include('admin.layouts.partials._head')
    @stack('styles')
    @stack('head_js')
</head>
<body>
    <header>
        <div class="container">
            @include('admin.layouts.partials._navbar')
        </div>
    </header>
    <div class="container">
        @yield('content')
    </div>
    <footer>
        @include('admin.layouts.partials._footer')
    </footer>
@stack('body_js')
</body>
</html>
