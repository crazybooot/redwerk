<nav class="navbar navbar-dark bg-primary">
    <ul class="nav navbar-nav">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ trans('admin.navigation.page.title') }}
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#">{{ trans('admin.navigation.page.list') }}</a>
                <a class="dropdown-item" href="#">{{ trans('admin.navigation.page.new') }}</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ trans('admin.navigation.slide.title') }}
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#">{{ trans('admin.navigation.slide.list') }}</a>
                <a class="dropdown-item" href="#">{{ trans('admin.navigation.slide.new') }}</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ trans('admin.navigation.header_link.title') }}
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#">{{ trans('admin.navigation.header_link.list') }}</a>
                <a class="dropdown-item" href="#">{{ trans('admin.navigation.header_link.new') }}</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ trans('admin.navigation.footer_link.title') }}
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#">{{ trans('admin.navigation.footer_link.list') }}</a>
                <a class="dropdown-item" href="#">{{ trans('admin.navigation.footer_link.new') }}</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ trans('admin.navigation.budget.title') }}
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#">{{ trans('admin.navigation.budget.list') }}</a>
                <a class="dropdown-item" href="#">{{ trans('admin.navigation.budget.new') }}</a>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">{{ trans('admin.navigation.get_in_touch.title') }}</a>
        </li>
    </ul>
</nav>