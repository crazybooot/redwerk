@extends('admin.layouts.main')
@section('title', trans('admin.page.index.page_title'))
@section('content')
    <div class="container">
        <h1>{{ trans('admin.page.index.body_title') }} </h1>
        <table class="table">
            <thead>
            <tr>
                <th>{{ trans('admin.page.index.table.headings.title') }}</th>
                <th>{{ trans('admin.page.index.table.headings.content') }}</th>
                <th>{{ trans('admin.page.index.table.headings.status') }}</th>
                <th>{{ trans('admin.page.index.table.headings.actions') }}</th>
            </tr>
            </thead>
            <tbody>
            @each('admin.page.partials._each', $pages, 'page')
            </tbody>
        </table>
    </div>
@endsection