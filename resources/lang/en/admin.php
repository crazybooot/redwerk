<?php

return [
    'navigation' => [
        'page'        => [
            'title' => 'Pages',
            'list'  => 'List',
            'new'   => 'New',
        ],
        'slide'       => [
            'title' => 'Slides',
            'list'  => 'List',
            'new'   => 'New',
        ],
        'header_link' => [
            'title' => 'Header Links',
            'list'  => 'List',
            'new'   => 'New',
        ],
        'footer_link' => [
            'title' => 'Footer Links',
            'list'  => 'List',
            'new'   => 'New',
        ],
        'budget' => [
            'title' => 'Budgets',
            'list'  => 'List',
            'new'   => 'New',
        ],
        'get_in_touch' => [
            'title' => 'Get In Touch Form',
        ],
    ],
    'page'       => [
        'index' => [
            'page_title' => 'Created pages',
            'body_title' => 'List of pages',
            'table'      => [
                'headings' => [
                    'title'   => 'Title',
                    'content' => 'Content',
                    'status'  => 'Status',
                    'actions' => 'Actions',
                ],
            ],
        ],
    ],
];
