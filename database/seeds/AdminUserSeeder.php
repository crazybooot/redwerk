<?php

use Illuminate\Database\Seeder;
use Redwerk\Models\User;

/**
 * Class AdminUserSeeder
 */
class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        factory(User::class)->create([
            'email' => 'test@example.com',
            'password' => bcrypt('password'),
        ]);
    }
}
