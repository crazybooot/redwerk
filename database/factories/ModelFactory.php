<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Redwerk\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name'           => $faker->name,
        'email'          => $faker->unique()->safeEmail,
        'password'       => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(Redwerk\Models\Slide::class, function (Faker\Generator $faker) {

    return [
        'title'        => $faker->sentence(),
        'text'         => $faker->text(50),
        'button_text'  => $faker->word,
        'button_link'  => $faker->url,
        'order'        => $faker->randomDigitNotNull,
        'is_published' => $faker->boolean,
    ];
});

$factory->define(Redwerk\Models\Page::class, function (Faker\Generator $faker) {

    return [
        'html_title'       => $faker->sentence(),
        'html_h1'          => $faker->sentence(),
        'meta_description' => $faker->sentence(),
        'meta_keywords'    => $faker->sentence(),
        'seo_url'          => str_slug($faker->sentence()),
        'title'            => $faker->sentence(),
        'content'          => $faker->text(100),
        'is_published'     => $faker->boolean,
    ];
});
